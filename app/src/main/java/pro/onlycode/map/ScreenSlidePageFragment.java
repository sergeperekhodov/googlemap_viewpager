package pro.onlycode.map;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pro.onlycode.map.Models.MarkerPlace;

@SuppressLint("ValidFragment")
public class ScreenSlidePageFragment extends Fragment {
    private String textplace;
    private List<MarkerPlace> markerPlaces = new ArrayList<>();

    @SuppressLint("ValidFragment")
    public ScreenSlidePageFragment(MarkerPlace markerPlaces) {
        this.textplace= markerPlaces.getPlaceTitle();
        this.markerPlaces.add(markerPlaces);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_screen_slide_page, container, false);

        ((TextView) rootView.findViewById(R.id.textplace)).setText(textplace);
        return rootView;
    }
}
