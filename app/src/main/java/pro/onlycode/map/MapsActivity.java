package pro.onlycode.map;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import pro.onlycode.map.Adapter.MarkerAdapter;
import pro.onlycode.map.Models.MarkerPlace;
import pro.onlycode.map.Models.Venue;
import pro.onlycode.map.Network.NetworkUtil;
import pro.onlycode.map.Network.ResponseFourSquare;
import pro.onlycode.map.Network.RetrofitInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    private int currentPage;
    private GoogleMap mMap;
    CompositeDisposable mSubscriptions;
    List<Venue> venues = new ArrayList<>();
    MarkerAdapter markerAdapter;
    List<MarkerPlace> places = new ArrayList<>();
    List<LatLng> markers = new ArrayList<>();
    CustomPager viewPager;
    PagerAdapter pagerAdapter;
    BottomSheetBehavior bottomSheetBehavior;
    PageListener pageListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mSubscriptions = new CompositeDisposable();
        viewPager = (CustomPager) findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(3);


        getPlaces();


    }


    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera
    }

    public void getPlaces() {
        RetrofitInterface retrofit = NetworkUtil.getRetrofit();
        Call call = retrofit.getPlaces(Constants.FOURSQUARE_CLIENT_KEY,
                Constants.FOURSQUARE_CLIENT_SECRET,
                "44.3,37.2",
                "Lower East Side, NY",
                "10",
                "20000",
                "20190513");
        call.enqueue(new Callback<ResponseFourSquare>() {
            @Override
            public void onResponse(Call<ResponseFourSquare> call, Response<ResponseFourSquare> response) {
                ResponseFourSquare response1 = response.body();
                venues = response1.getResponse().getVenues();
                for (int i = 0; i < venues.size(); i++) {
                    MarkerPlace markerPlace = new MarkerPlace();
                    markerPlace.setLat(venues.get(i).getLocation().getLat());
                    markerPlace.setLng(venues.get(i).getLocation().getLng());
                    markerPlace.setPlaceTitle(venues.get(i).getName());
                    LatLng marker = new LatLng(markerPlace.getLat(), markerPlace.getLng());
                    mMap.addMarker(new MarkerOptions().position(marker).title(markerPlace.getPlaceTitle()));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(marker));
                    places.add(markerPlace);
                    markers.add(marker);

                    markerAdapter = new MarkerAdapter(places);
                    pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(), places);
                    viewPager.setAdapter(pagerAdapter);
                    pageListener = new PageListener(places, mMap,markers);
                    viewPager.setOnPageChangeListener(pageListener);

                }

            }

            @Override
            public void onFailure(Call<ResponseFourSquare> call, Throwable t) {
                Toast.makeText(MapsActivity.this, t.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
        });



    }
}
