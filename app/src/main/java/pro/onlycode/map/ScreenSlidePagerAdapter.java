package pro.onlycode.map;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import java.util.List;

import pro.onlycode.map.Models.MarkerPlace;

public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
    List<MarkerPlace> places;
    public ScreenSlidePagerAdapter(FragmentManager fm, List<MarkerPlace> places) {
        super(fm);
        this.places = places;
    }

    @Override
    public Fragment getItem(int position) {
        return new ScreenSlidePageFragment(places.get(position));
    }

    @Override
    public int getCount() {
        return places.size();
    }
}