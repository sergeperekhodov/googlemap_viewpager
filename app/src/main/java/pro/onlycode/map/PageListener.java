package pro.onlycode.map;

import android.support.v4.view.ViewPager;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import pro.onlycode.map.Models.MarkerPlace;

import static android.content.ContentValues.TAG;

public class PageListener extends ViewPager.SimpleOnPageChangeListener {
    int currentPos;
    List<MarkerPlace> markerPlace;
    GoogleMap map;
    List<LatLng> markers;
    public PageListener(List<MarkerPlace> markerPlace, GoogleMap map, List<LatLng> markers) {
        this.markerPlace = markerPlace;
        this.map = map;
        this.markers = markers;
    }

    public void onPageSelected(int position) {
        Log.i(TAG, "page selected " + position);
        currentPos =  position;
        map.moveCamera(CameraUpdateFactory.newLatLng(markers.get(position)));
    }
}
