package pro.onlycode.map.Network;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RetrofitInterface {

    @GET("venues/trending")
    Call<ResponseFourSquare> getPlaces(
            @Query("client_id") String client_id,
            @Query("client_secret") String client_secret,
            @Query("ll") String ll,
            @Query("near") String near,
            @Query("limit") String limit,
            @Query("radius") String radius,
            @Query("v") String v
            );
}
